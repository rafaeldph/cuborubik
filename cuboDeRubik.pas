program cuboDeRubik;
uses crt;
type
  movimiento = record
    cadena: string;
    arreglo: array[0..9] of string;
  end;
var
  i, j, n: integer;
  cara1, cara2, cara3, cara4, cara5, cara6: array[0..2,0..2] of integer;
  aux: array[0..2] of integer;
  jugadas: movimiento;
  respuesta: char;

procedure separarmovimientos;
{Al usuario se le pedira una cadena que puede contener hasta 10 movimientos}
{Este procedimiento separa la cadena, creando un arreglo de cadenas (cada una es un movimiento)}
begin
  for i:=0 to 9 do
    jugadas.arreglo[i] := '';
    {Durante el programa el arreglo se llena
    Si en algun momento el usuario proporciona una cantidad de movimientos menor a 10, para evitar que queden guardados los movimientos anteriores, primero se vacia el arreglo}
  i := 0;
  j := 0;
  repeat
    j := j + 1;
    {Espacio es la forma de separar los movimientos}
    if (jugadas.cadena[j] = ' ') and (jugadas.cadena[j-1] <> ' ') then {Se comprueba que la posicion anterior tampoco sea un espacio, para evitar que se salten dos o mas posiciones si el usuario ingresa mas de un caracter de espacio}
      i := i + 1 {Por cada espacio, se va a la siguiente posicion del arreglo}
    else
      jugadas.arreglo[i] := jugadas.arreglo[i] + jugadas.cadena[j]; {Si no es espacio, se sigue guardando letra por letra}
  until (j = length(jugadas.cadena));
end;

{Los procedimientos siguientes son movimientos del cubo, sus piezas se intercambian de una manera especifica}
{Los movimientos del cubo de Rubik se nombran con sus iniciales en ingles y toman como referencia el movimiento de las agujas del reloj (si la capa indicada a mover estuviera de frente al jugador)}
{En algunos casos (capas externas, que tienen piezas correspondientes a una cara entera) se hace uso de un arreglo auxiliar, debido a que las piezas rotan entre si}

procedure B; {Back}
begin
  for i := 0 to 2 do
    aux[i] := cara1[0,i];
  cara1[0,0] := cara4[0,2];
  cara1[0,1] := cara4[1,2];
  cara1[0,2] := cara4[2,2];
  cara4[0,2] := cara6[2,2];
  cara4[1,2] := cara6[2,1];
  cara4[2,2] := cara6[2,0];
  cara6[2,2] := cara2[2,0];
  cara6[2,1] := cara2[1,0];
  cara6[2,0] := cara2[0,0];
  cara2[2,0] := aux[0];
  cara2[1,0] := aux[1];
  cara2[0,0] := aux[2];
  for i := 0 to 2 do
    aux[i] := cara5[0,i];
  cara5[0,1] := cara5[1,0];
  cara5[0,0] := cara5[2,0];
  cara5[1,0] := cara5[2,1];
  cara5[2,0] := cara5[2,2];
  cara5[2,1] := cara5[1,2];
  cara5[2,2] := aux[2];
  cara5[1,2] := aux[1];
  cara5[0,2] := aux[0];
end;

procedure D; {Down}
begin
  for i := 0 to 2 do
    begin
      aux[i] := cara3[2,i];
      cara3[2,i] := cara2[2,i];
      cara2[2,i] := cara5[2,i];
      cara5[2,i] := cara4[2,i];
      cara4[2,i] := aux[i];
    end;
  for i := 0 to 2 do
    aux[i] := cara6[0,i];
  cara6[0,1] := cara6[1,0];
  cara6[0,0] := cara6[2,0];
  cara6[1,0] := cara6[2,1];
  cara6[2,0] := cara6[2,2];
  cara6[2,1] := cara6[1,2];
  cara6[2,2] := aux[2];
  cara6[1,2] := aux[1];
  cara6[0,2] := aux[0];
end;

procedure E; {Equator}
begin
  for i := 0 to 2 do
    begin
      aux[i] := cara3[1,i];
      cara3[1,i] := cara2[1,i];
      cara2[1,i] := cara5[1,i];
      cara5[1,i] := cara4[1,i];
      cara4[1,i] := aux[i];
    end;
end;

procedure F; {Front}
begin
  for i := 0 to 2 do
    aux[i] := cara1[2,i];
  cara1[2,2] := cara2[0,2];
  cara1[2,1] := cara2[1,2];
  cara1[2,0] := cara2[2,2];
  cara2[0,2] := cara6[0,0];
  cara2[1,2] := cara6[0,1];
  cara2[2,2] := cara6[0,2];
  cara6[0,0] := cara4[2,0];
  cara6[0,1] := cara4[1,0];
  cara6[0,2] := cara4[0,0];
  cara4[2,0] := aux[2];
  cara4[1,0] := aux[1];
  cara4[0,0] := aux[0];
  for i := 0 to 2 do
    aux[i] := cara3[0,i];
  cara3[0,1] := cara3[1,0];
  cara3[0,0] := cara3[2,0];
  cara3[1,0] := cara3[2,1];
  cara3[2,0] := cara3[2,2];
  cara3[2,1] := cara3[1,2];
  cara3[2,2] := aux[2];
  cara3[1,2] := aux[1];
  cara3[0,2] := aux[0];
end;

procedure L; {Left}
begin
  for i := 0 to 2 do
    aux[i] := cara3[i,0];
  cara3[2,0] := cara1[2,0];
  cara3[1,0] := cara1[1,0];
  cara3[0,0] := cara1[0,0];
  cara1[2,0] := cara5[0,2];
  cara1[1,0] := cara5[1,2];
  cara1[0,0] := cara5[2,2];
  cara5[0,2] := cara6[2,0];
  cara5[1,2] := cara6[1,0];
  cara5[2,2] := cara6[0,0];
  cara6[2,0] := aux[2];
  cara6[1,0] := aux[1];
  cara6[0,0] := aux[0];
  for i := 0 to 2 do
    aux[i] := cara2[0,i];
  cara2[0,1] := cara2[1,0];
  cara2[0,0] := cara2[2,0];
  cara2[1,0] := cara2[2,1];
  cara2[2,0] := cara2[2,2];
  cara2[2,1] := cara2[1,2];
  cara2[2,2] := aux[2];
  cara2[1,2] := aux[1];
  cara2[0,2] := aux[0];
end;

procedure M; {Middle}
begin
  for i := 0 to 2 do
    aux[i] := cara3[i,1];
  cara3[2,1] := cara1[2,1];
  cara3[1,1] := cara1[1,1];
  cara3[0,1] := cara1[0,1];
  cara1[2,1] := cara5[0,1];
  cara1[1,1] := cara5[1,1];
  cara1[0,1] := cara5[2,1];
  cara5[0,1] := cara6[2,1];
  cara5[1,1] := cara6[1,1];
  cara5[2,1] := cara6[0,1];
  cara6[2,1] := aux[2];
  cara6[1,1] := aux[1];
  cara6[0,1] := aux[0];
end;

procedure R; {Right}
begin
  for i := 0 to 2 do
    aux[i] := cara3[i,2];
  cara3[0,2] := cara6[0,2];
  cara3[1,2] := cara6[1,2];
  cara3[2,2] := cara6[2,2];
  cara6[0,2] := cara5[2,0];
  cara6[1,2] := cara5[1,0];
  cara6[2,2] := cara5[0,0];
  cara5[2,0] := cara1[0,2];
  cara5[1,0] := cara1[1,2];
  cara5[0,0] := cara1[2,2];
  cara1[0,2] := aux[0];
  cara1[1,2] := aux[1];
  cara1[2,2] := aux[2];
  for i := 0 to 2 do
    aux[i] := cara4[0,i];
  cara4[0,1] := cara4[1,0];
  cara4[0,0] := cara4[2,0];
  cara4[1,0] := cara4[2,1];
  cara4[2,0] := cara4[2,2];
  cara4[2,1] := cara4[1,2];
  cara4[2,2] := aux[2];
  cara4[1,2] := aux[1];
  cara4[0,2] := aux[0];
end;

procedure S; {Standing}
begin
  for i := 0 to 2 do
    aux[i] := cara4[i,1];
  cara4[2,1] := cara1[1,2];
  cara4[1,1] := cara1[1,1];
  cara4[0,1] := cara1[1,0];
  cara1[1,2] := cara2[0,1];
  cara1[1,1] := cara2[1,1];
  cara1[1,0] := cara2[2,1];
  cara2[0,1] := cara6[1,0];
  cara2[1,1] := cara6[1,1];
  cara2[2,1] := cara6[1,2];
  cara6[1,0] := aux[2];
  cara6[1,1] := aux[1];
  cara6[1,2] := aux[0];
end;

procedure U; {Up}
begin
  for i := 0 to 2 do
    begin
      aux[i] := cara3[0,i];
      cara3[0,i] := cara4[0,i];
      cara4[0,i] := cara5[0,i];
      cara5[0,i] := cara2[0,i];
      cara2[0,i] := aux[i];
    end;
  for i := 0 to 2 do
    aux[i] := cara1[0,i];
  cara1[0,1] := cara1[1,0];
  cara1[0,0] := cara1[2,0];
  cara1[1,0] := cara1[2,1];
  cara1[2,0] := cara1[2,2];
  cara1[2,1] := cara1[1,2];
  cara1[2,2] := aux[2];
  cara1[1,2] := aux[1];
  cara1[0,2] := aux[0];
end;

procedure inicializar; {Se rellena el cubo con los colores correspondientes}
begin
  for i:=0 to 2 do
    for j:=0 to 2 do
      begin
        cara1[i,j] := 7; {Blanco}
        cara2[i,j] := 2; {Verde}
        cara3[i,j] := 4; {Rojo}
        cara4[i,j] := 1; {Azul}
        cara5[i,j] := 13; {LightMagenta (porque no hay naranja)}
        cara6[i,j] := 6; {Amarillo}
      end;
end;

procedure identificacion; {Se usa en lugar de clrscr cada vez que se borra la pantalla, asi se identifica el programa y los integrantes del grupo}
begin
  clrscr;
  textcolor(lightgreen);
  writeln('                                                       Cubo de Rubik');
  gotoxy(108,28);
  write('Adrian Alves');
  gotoxy(108,29);
  write('Rafael Perez');
  gotoxy(92,30);
  write('Programacion I - seccion T01');
  gotoxy(1,5);
  textcolor(white);
end;

procedure escribir(pieza: integer);
begin
  {Las caras del cubo son un arreglo de caracteres, donde cada caracter indica el codigo correspondiente al fondo de pantalla para la pieza de cada color}
  textbackground(pieza); {Se cambia al color de la pieza}
  write('  '); {Cada pieza es dos espacios en el color almacenado en el arreglo}
  textbackground(black); {Y luego se regresa al color negro (fondo estandar)}
  write(' '); {Se deja un espacio entre dos piezas}
end;

procedure escribirCaras;
begin
  identificacion;
  for i:=0 to 2 do {Se escriben tres lineas de la cara 1}
    begin
      write('           '); escribir(cara1[i,0]); escribir(cara1[i,1]); escribir(cara1[i,2]); writeln; writeln; {Se deja un espacio entre lineas}
    end;
  for i:=0 to 2 do
    begin
      write(' ');
      escribir(cara2[i,0]); escribir(cara2[i,1]); escribir(cara2[i,2]); {Se escriben las tres piezas de cada linea horizontal de cada cara}
      write(' '); {Entre dos caras diferentes se deja un espacio extra}
      escribir(cara3[i,0]); escribir(cara3[i,1]); escribir(cara3[i,2]);
      write(' ');
      escribir(cara4[i,0]); escribir(cara4[i,1]); escribir(cara4[i,2]);
      write(' ');
      escribir(cara5[i,0]); escribir(cara5[i,1]); escribir(cara5[i,2]);
      writeln; writeln;
    end;
  for i:=0 to 2 do
    begin
      write('           '); escribir(cara6[i,0]); escribir(cara6[i,1]); escribir(cara6[i,2]); writeln; writeln;
    end;
end;

procedure mezclar; {Para mezclar el cubo, se realiza cada procedimiento de los movimientos un numero aleatorio de veces}
var n: integer;
begin
  n := random(5);
  for j:= 0 to n do
    B;
  n := random(5);
  for j:= 0 to n do
    D;
  n := random(5);
  for j:= 0 to n do
    E;
  n := random(5);
  for j:= 0 to n do
    F;
  n := random(5);
  for j:= 0 to n do
    L;
  n := random(5);
  for j:= 0 to n do
    M;
  n := random(5);
  for j:= 0 to n do
    R;
  n := random(5);
  for j:= 0 to n do
    S;
  n := random(5);
  for j:= 0 to n do
    U;
end;

procedure movimientos(movimiento: string); {Movimientos validos}
begin
  {Los movimientos simples son el correspondiente a cada procedimiento}
  {Con un 2 se indica que el movimiento se realiza 2 veces}
  {Las comillas se usan para realizar el movimiento en sentido contrario. Realizar un movimiento en sentido contrario equivale a realizarlo tres veces en la direccion original del procedimiento}
  {Los movimientos con letras minusculas indican usar dos capas (son validos solo para exteriores). Por ejemplo, el movimiento l equivale a realizar a la vez los movimientos L y M}
  case movimiento of
    'B': B;
    'B"': begin B; B; B; end;
    'B2': begin B; B; end;
    '2B': begin B; B; end;
    'b': begin B; S; S; S; end;
    'b"': begin B; B; B; S; end;
    'b2': begin B; B; S; S; end;
    '2b': begin B; B; S; S; end;

    'D': D;
    'D"': begin D; D; D; end;
    'D2': begin D; D; end;
    '2D': begin D; D; end;
    'd': begin D; E; end;
    'd"': begin D; D; D; E; E; E; end;
    'd2': begin D; D; E; E; end;
    '2d': begin D; D; E; E; end;

    'E': E;
    'E"': begin E; E; E; end;
    'E2': begin E; E; end;
    '2E': begin E; E; end;

    'F': F;
    'F"': begin F; F; F; end;
    '2F': begin F; F; end;
    'F2': begin F; F; end;
    'f': begin F; S; end;
    'f"': begin F; F; F; S; S; S; end;
    '2f': begin F; F; S; S; end;
    'f2': begin F; F; S; S; end;

    'L': L;
    'L"': begin L; L; L; end;
    'L2': begin L; L; end;
    '2L': begin L; L; end;
    'l': begin L; M; end;
    'l"': begin L; L; L; M; M; M; end;
    'l2': begin L; L; M; M; end;
    '2l': begin L; L; M; M; end;

    'M': M;
    'M"': begin M; M; M; end;
    'M2': begin M; M; end;
    '2M': begin M; M; end;

    'R': R;
    'R"': begin R; R; R; end;
    'R2': begin R; R; end;
    '2R': begin R; R; end;
    'r': begin R; M; M; M; end;
    'r"': begin R; R; R; M; end;
    'r2': begin R; R; M; M; end;
    '2r': begin R; R; M; M; end;

    'S': S;
    'S"': begin S; S; S; end;
    '2S': begin S; S; end;
    'S2': begin S; S; end;

    'U': U;
    'U"': begin U; U; U; end;
    'U2': begin U; U; end;
    '2U': begin U; U; end;
    'u': begin U; M; M; M; end;
    'u2': begin U; U; M; M; end;
    '2u': begin U; U; M; M; end;
    'u"': begin U; U; U; M; end;

    'X': begin R; L; L; L; M; M; M; end;
    'X"': begin R; R; R; L; M; end;
    'X2': begin R; R; L; L; M; M; end;
    '2X': begin R; R; L; L; M; M; end;

    'Y': begin B; F; F; F; S; S; S; end;
    'Y"': begin B; B; B; F; S; end;
    'Y2': begin B; B; F; F; S; S; end;
    '2Y': begin B; B; F; F; S; S; end;

    'Z': begin U; E; E; E; D; D; D; end;
    'Z"': begin U; U; U; E; D; end;
    'Z2': begin U; U; E; E; D; D; end;
    '2Z': begin U; U; E; E; D; D; end;

    {Easter egg: sexy move es un conjunto de movimientos (R, U, R', U')}
    'Sexy': begin R; U; R; R; R; U; U; U; end;
    'SEXY': begin R; U; R; R; R; U; U; U; end;
    'sexy': begin R; U; R; R; R; U; U; U; end;
    'Sexy move': begin R; U; R; R; R; U; U; U; end;
    'SEXY MOVE': begin R; U; R; R; R; U; U; U; end;
    'sexy move': begin R; U; R; R; R; U; U; U; end;
  else
    if (movimiento <> '') then
      writeln('El movimiento ', n+1, ' no es una jugada valida'); {Los movimientos se piden en bloques de maximo 10, por lo tanto se indica el numero del movimiento invalido}
  end;
end;

function armado: boolean;
var contador: integer;
begin
  contador := 0;
  {Se necesita que todas las piezas de una cara sean iguales para que este armada. Por lo tanto, la funcion armado solo evalua en base a la primera pieza de cada cara}
  for i:=0 to 2 do
    for j:=0 to 2 do
      if (cara1[i,j] <> cara1[0,0]) then
        contador := contador + 1;  {Por cada pieza diferente a la primera de cada cara, se incrementa un contador}
  for i:=0 to 2 do
    for j:=0 to 2 do
      if (cara2[i,j] <> cara2[0,0]) then
        contador := contador + 1;
  for i:=0 to 2 do
    for j:=0 to 2 do
      if (cara3[i,j] <> cara3[0,0]) then
        contador := contador + 1;
  for i:=0 to 2 do
    for j:=0 to 2 do
      if (cara4[i,j] <> cara4[0,0]) then
        contador := contador + 1;
  for i:=0 to 2 do
    for j:=0 to 2 do
      if (cara5[i,j] <> cara5[0,0]) then
        contador := contador + 1;
  for i:=0 to 2 do
    for j:=0 to 2 do
      if (cara6[i,j] <> cara6[0,0]) then
        contador := contador + 1;
  if (contador > 0) then {Si existe al menos una pieza fuera de lugar, el cubo esta desarmado}
    armado := false
  else
    armado := true; {De lo contrario, el cubo esta armado}
end;

procedure instrucciones;
begin
  identificacion;
  writeln('Bienvenido al cubo de Rubik digital en 2 dimensiones');
  writeln('El cubo se muestra por sus caras de forma abierta, como una cruz');
  writeln('Los movimientos se realizan a traves de instrucciones por teclado, mediante la notacion Singmaster');
  writeln;
  writeln('Movimientos validos:');
  writeln('  Movimientos de capas externas: F, B, U, D, R, L');
  writeln('  Movimientos de capas intermedias: M, E, S');
  writeln('  Rotaciones: X, Y, Z');
  writeln('Se reconocen las letras minusculas como movimientos de dos capas (solo para exteriores)');
  writeln('Para mover en sentido contrario, se hace uso de comillas (")');
  writeln('Se indica con un "2" al principio para hacer un movimiento dos veces');
  writeln;
  writeln('Los movimientos se separan mediante espacios');
  writeln('Se puede realizar hasta 10 movimientos a la vez');
  write('Pulse cualquier tecla para continuar...');
  readkey;
end;

procedure final_ganador;
begin
  for i:=1 to 15 do
    begin
      identificacion;
      textcolor(i);
      gotoxy(1,13);
      writeln('                                                    __________________');
      writeln('                                                   |                  |');
      writeln('                                                   |      GANASTE     |');
      writeln('                                                   |__________________|');
      delay(300);
    end;
end;

procedure final_perdedor;
begin
  identificacion;
  textcolor(lightgreen);
  gotoxy(1,13);
  writeln('                                                    ___________________');
  writeln('                                                   |                   |');
  writeln('                                                   | No lo lograste :( |');
  writeln('                                                   |___________________|');
  delay(2000);
end;

begin
  inicializar;
  instrucciones;
  escribirCaras;
  write('Pulse cualquier tecla para mezclar...');
  readkey;
  mezclar;
  escribirCaras;
  repeat
    write('Escriba los movimientos que desea realizar: ');
    readln(jugadas.cadena);
    separarmovimientos;
    for n:=0 to 9 do
      begin
        if (jugadas.arreglo[n] = '') then {Si ya no hay movimientos que hacer, sale del ciclo}
          break;
        movimientos(jugadas.arreglo[n]); {Se realiza el movimiento, o se escribira en pantalla que no es una opcion valida}
        delay(1000); {Para poder visualizar cada uno de los movimientos, se retarda cada uno 1 segundo}
        escribirCaras;
      end;
    repeat
      write('Desea continuar? S/N: '); {Se le pregunta al usuario si quiere seguir jugando}
      readln(respuesta);
      if (respuesta <> 's') and (respuesta <> 'S') and (respuesta <> 'n') and (respuesta <> 'N') then
        writeln('Respuesta incorrecta');
    until (respuesta = 's') or (respuesta = 'S') or (respuesta = 'n') or (respuesta = 'N');
  until (armado = true) or (respuesta = 'n') or (respuesta = 'N'); {Las dos formas de terminar el juego son armando el cubo o no seguir jugando}
  {Mostrar el final de acuerdo a la forma de terminar el juego}
  if (armado = true) then
    final_ganador
  else
    final_perdedor;
end.
